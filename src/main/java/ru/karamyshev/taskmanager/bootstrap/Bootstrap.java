package ru.karamyshev.taskmanager.bootstrap;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import ru.karamyshev.taskmanager.api.endpoint.*;
import ru.karamyshev.taskmanager.api.repository.*;
import ru.karamyshev.taskmanager.api.service.*;
import ru.karamyshev.taskmanager.command.AbstractCommand;
import ru.karamyshev.taskmanager.constant.MsgCommandConst;
import ru.karamyshev.taskmanager.endpoint.*;
import ru.karamyshev.taskmanager.enumerated.Role;
import ru.karamyshev.taskmanager.exception.CommandIncorrectException;
import ru.karamyshev.taskmanager.repository.*;
import ru.karamyshev.taskmanager.service.*;
import ru.karamyshev.taskmanager.util.TerminalUtil;

import javax.xml.ws.Endpoint;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

public class Bootstrap implements IServiceLocator {

    @NotNull
    private final IUserRepository userRepository = new UserRepository();

    @NotNull
    private final ICommandRepository commandRepository = new CommandRepository();

    @NotNull
    private final ITaskRepository taskRepository = new TaskRepository();

    @NotNull
    private final IProjectRepository projectRepository = new ProjectRepository();

    @NotNull
    private final ISessionRepository sessionRepository = new SessionRepository();

    @NotNull
    private final IUserService userService = new UserService(userRepository);

    @NotNull
    private final IAuthenticationService authenticationService = new AuthenticationService(userService);

    @NotNull
    private final ICommandService commandService = new CommandService(commandRepository);

    @NotNull
    private final ITaskService taskService = new TaskService(taskRepository);

    @NotNull
    private final IProjectService projectService = new ProjectService(projectRepository);

    @NotNull
    private final IDomainService domainService = new DomainService(userService, taskService, projectService);

    private final Map<String, AbstractCommand> commands = new LinkedHashMap<>();
    private final Map<String, AbstractCommand> arguments = new LinkedHashMap<>();

    @NotNull
    private final IPropertyService propertyService = new PropertyService();

    @NotNull
    private final ISessionService sessionService = new SessionService(sessionRepository, this);

    @NotNull
    private final IAdminService adminService = new AdminService(this);

    @NotNull
    private final ITaskEndpoint taskEndpoint = new TaskEndpoint(this);

    @NotNull
    private final IAdminEndpoint adminEndpoint = new AdminEndpoint(this);

    @NotNull
    private final IAdminUserEndpoint adminUserEndpoint = new AdminUserEndpoint(this);

    @NotNull
    private final IProjectEndpoint projectEndpoint = new ProjectEndpoint(this);

    @NotNull
    private final IUserEndpoint userEndpoint = new UserEndpoint(this);

    @NotNull
    private final ISessionEndpoint sessionEndpoint = new SessionEndpoint(this);

    @NotNull
    private final IAuthenticationEndpoint authenticationEndpoint = new AuthenticationEndpoint(this);

    @NotNull
    private final String url = "http://" + propertyService.getServerHost() + ":" + propertyService.getServerPort();

    @NotNull
    private final String sessionEndpointUrl = url + "/SessionEndpoint?wsdl";

    @NotNull
    private final String taskEndpointUrl = url + "/TaskEndpoint?wsdl";

    @NotNull
    private final String projectEndpointUrl = url + "/ProjectEndpoint?wsdl";

    @NotNull
    private final String userEndpointUrl = url + "/UserEndpoint?wsdl";

    @NotNull
    private final String adminUserEndpointUrl = url + "/AdminUserEndpoint?wsdl";

    @NotNull
    private final String adminDataEndpointUrl = url + "/AdminEndpoint?wsdl";

    @NotNull
    private final String authenticationDataEndpoint = url + "/AuthenticationEndpoint?wsdl";


    private void initEndpoint() {
        Endpoint.publish(authenticationDataEndpoint, authenticationEndpoint);
        Endpoint.publish(taskEndpointUrl, taskEndpoint);
        Endpoint.publish(userEndpointUrl, userEndpoint);
        Endpoint.publish(projectEndpointUrl, projectEndpoint);
        Endpoint.publish(sessionEndpointUrl, sessionEndpoint);
        Endpoint.publish(adminUserEndpointUrl, adminUserEndpoint);
        Endpoint.publish(adminDataEndpointUrl, adminEndpoint);
    }

    private void showEndpoint() {
        System.out.println(sessionEndpointUrl);
        System.out.println(taskEndpointUrl);
        System.out.println(projectEndpointUrl);
        System.out.println(userEndpointUrl);
        System.out.println(adminUserEndpointUrl);
        System.out.println(adminDataEndpointUrl);
        System.out.println(authenticationDataEndpoint);
    }

    {
        init(commandService.getTerminalCommands());
    }

    private void init(List<AbstractCommand> commands) {
        for (AbstractCommand command : commands) {
            registry(command);
        }
    }

    private void initProperty() {
        propertyService.init();
    }


    private void registry(final AbstractCommand command) {
        if (command == null) return;
        command.setServiceLocator(this);
        commands.put(command.name(), command);
        arguments.put(command.arg(), command);
    }

    private void initUser() {
        userService.create("test", "test", "test@test.com");
        userService.create("admin", "admin", Role.ADMIN);
    }

    public void run(final String[] args) {
        initProperty();
        initEndpoint();
        initUser();
        System.out.println("*** WELCOME TO TASK MANAGER ***");
        showEndpoint();
        parsArgs(args);
        inputCommand();
    }

    private void inputCommand() {
        while (true) {
            try {
                parsCommand(TerminalUtil.nextLine());
            } catch (Exception e) {
                System.err.println(e.getMessage());
                System.err.println("[FAIL]");
            }
        }
    }

    private void parsCommand(final String args) throws Exception {
        validateArgs(args);
        String[] command = args.trim().split("\\s+");
        for (String arg : command) chooseResponsCommand(arg);
    }

    private void parsArgs(final String... args) {
        validateArgs(args);
        try {
            for (String arg : args) chooseResponsArg(arg.trim());
        } catch (Exception e) {
            System.err.println(e.getMessage());
            System.err.println("[FAIL]");
        }

    }

    private void validateArgs(final String... args) {
        if (args != null || args.length > 0) return;
        System.out.println(MsgCommandConst.COMMAND_ABSENT);
    }

    @SneakyThrows
    private void chooseResponsArg(final String arg) {
        final AbstractCommand argument = arguments.get(arg);
        if (argument == null) throw new CommandIncorrectException(arg);
        argument.execute();
    }

    @SneakyThrows
    private void chooseResponsCommand(final String cmd) {
        final AbstractCommand command = commands.get(cmd);
        if (command == null) throw new CommandIncorrectException(cmd);
        authenticationService.checkRoles(command.roles());
        command.execute();
    }

    @NotNull
    @Override
    public IUserService getUserService() {
        return userService;
    }

    @NotNull
    @Override
    public IAuthenticationService getAuthenticationService() {
        return authenticationService;
    }

    @NotNull
    @Override
    public ICommandService getCommandService() {
        return commandService;
    }

    @NotNull
    @Override
    public ITaskService getTaskService() {
        return taskService;
    }

    @NotNull
    @Override
    public IProjectService getProjectService() {
        return projectService;
    }

    @NotNull
    @Override
    public IDomainService getDomainService() {
        return domainService;
    }

    @NotNull
    @Override
    public ISessionService getSessionService() {
        return sessionService;
    }

    @NotNull
    @Override
    public IPropertyService getPropertyService() {
        return propertyService;
    }

    @NotNull
    @Override
    public IAdminService getAdminService() {
        return adminService;
    }

}
