package ru.karamyshev.taskmanager.service;

import org.jetbrains.annotations.Nullable;
import ru.karamyshev.taskmanager.api.repository.ISessionRepository;
import ru.karamyshev.taskmanager.api.service.IPropertyService;
import ru.karamyshev.taskmanager.api.service.IServiceLocator;
import ru.karamyshev.taskmanager.api.service.ISessionService;
import ru.karamyshev.taskmanager.entity.Session;
import ru.karamyshev.taskmanager.entity.User;
import ru.karamyshev.taskmanager.enumerated.Role;
import ru.karamyshev.taskmanager.util.HashUtil;
import ru.karamyshev.taskmanager.util.SignatureUtil;

import javax.validation.constraints.NotNull;
import java.util.List;

public class SessionService extends AbstractService<Session> implements ISessionService {

    private final IServiceLocator serviceLocator;

    private final ISessionRepository sessionRepository;

    public SessionService(final ISessionRepository sessionRepository, final IServiceLocator serviceLocator) {
        super(sessionRepository);
        this.serviceLocator = serviceLocator;
        this.sessionRepository = sessionRepository;
    }

    @Override
    public void close(@Nullable final Session session) throws Exception {
        validate(session);
        sessionRepository.removeByUserId(session.getUserId());
    }

    @Override
    public void closeAll(@Nullable final Session session) throws Exception {
        validate(session);
        sessionRepository.removeByUserId(session.getUserId());
    }

    @Nullable
    @Override
    public User getUser(@Nullable final Session session) throws Exception {
        final String userId = getUserId(session);
        return serviceLocator.getUserService().findById(userId);
    }

    @Nullable
    @Override
    public String getUserId(@Nullable final Session session) throws Exception {
        validate(session);
        return session.getUserId();
    }

    @Nullable
    @Override
    public List<Session> getListSession(@Nullable final Session session) throws Exception {
        validate(session);
        return sessionRepository.findAllSession(session.getUserId());
    }

    @Nullable
    @Override
    public Session sign(@Nullable final Session session) {
        if (session == null) return null;
        session.setSignature(null);
        final IPropertyService propertyService = serviceLocator.getPropertyService();
        final String salt = propertyService.getSessionSalt();
        final Integer cycle = propertyService.getSessionCycle();
        final String signature = SignatureUtil.sign(session, salt, cycle);
        session.setSignature(signature);
        return session;
    }

    @Override
    public boolean isValid(@Nullable final Session session) {
        try {
            validate(session);
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    @Override
    public void validate(@Nullable final Session session) throws Exception {
        if (session == null) throw new NullPointerException();
        if (session.getSignature() == null || session.getSignature().isEmpty()) throw new NullPointerException();
        if (session.getUserId() == null || session.getUserId().isEmpty()) throw new NullPointerException();
        if (session.getTimestamp() == null) throw new NullPointerException();
        final Session temp = session.clone();
        if (temp == null) throw new NullPointerException();
        final String signatureSource = session.getSignature();
        final String signatureTarget = sign(temp).getSignature();
        final boolean check = signatureSource.equals(signatureTarget);
        if (!check) throw new NullPointerException();
        if (sessionRepository.contains(session.getUserId())) ;
    }

    @Override
    public void validate(@Nullable final Session session, @Nullable final Role role) throws Exception {
        if (role == null) throw new NullPointerException();
        validate(session);
        final String userId = session.getUserId();
        final User user = serviceLocator.getUserService().findById(userId);
        if (user == null) throw new NullPointerException();
        if (user.getRole() == null) throw new NullPointerException();
        if (!role.equals(user.getRole())) throw new NullPointerException();
    }

    @Nullable
    @Override
    public Session open(@NotNull final String login, @NotNull final String password) {
        final boolean check = checkDataAccess(login, password);
        if (!check) return null;
        final User user = serviceLocator.getUserService().findByLogin(login);
        if (user == null) return null;
        final Session session = new Session();
        session.setUserId(Long.toString(user.getId()));
        session.setTimestamp(System.currentTimeMillis());
        sessionRepository.add(session);
        return sign(session);
    }

    @Override
    public boolean checkDataAccess(@Nullable final String login, @Nullable final String password) {
        if (login == null || login.isEmpty()) return false;
        if (password == null || password.isEmpty()) return false;
        final User user = serviceLocator.getUserService().findByLogin(login);
        if (user == null) return false;
        final String passwordHash = HashUtil.md5(password);
        if (passwordHash == null || passwordHash.isEmpty()) return false;
        return passwordHash.equals(user.getPasswordHash());
    }

    @Override
    public void signOutByLogin(final String login) throws Exception {
        if (login == null || login.isEmpty()) return;
        final User user = serviceLocator.getUserService().findByLogin(login);
        if (user == null) return;
        final String userId = Long.toString(user.getId());
        sessionRepository.removeByUserId(userId);
    }

    @Override
    public void signOutByUserId(@Nullable final String userId) throws Exception {
        if (userId == null || userId.isEmpty()) return;
        sessionRepository.removeByUserId(userId);
    }

}
