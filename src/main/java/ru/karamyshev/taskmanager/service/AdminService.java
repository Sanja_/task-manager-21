package ru.karamyshev.taskmanager.service;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.dataformat.xml.XmlMapper;
import org.eclipse.persistence.jaxb.MarshallerProperties;
import org.eclipse.persistence.jaxb.UnmarshallerProperties;
import org.jetbrains.annotations.NotNull;
import ru.karamyshev.taskmanager.api.service.IAdminService;
import ru.karamyshev.taskmanager.api.service.IServiceLocator;
import ru.karamyshev.taskmanager.constant.DataConstant;
import ru.karamyshev.taskmanager.dto.Domain;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;
import java.io.*;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Base64;

public class AdminService implements IAdminService {

    private final IServiceLocator serviceLocator;

    public AdminService(final IServiceLocator serviceLocator) {
        this.serviceLocator = serviceLocator;
    }

    @Override
    public void loadBinary() throws Exception {
        final FileInputStream fileInputStream = new FileInputStream(DataConstant.FILE_BINARY);
        final ObjectInputStream objectInputStream = new ObjectInputStream(fileInputStream);
        final Domain domain = (Domain) objectInputStream.readObject();
        serviceLocator.getDomainService().load(domain);
    }

    @Override
    public void saveBinary() throws Exception {
        @NotNull final Domain domain = prepareData();
        final File file = createSaveFile(DataConstant.FILE_BINARY);

        final FileOutputStream fileOutputStream = new FileOutputStream(file);
        final ObjectOutputStream objectOutputStream = new ObjectOutputStream(fileOutputStream);
        objectOutputStream.writeObject(domain);
        objectOutputStream.writeObject(domain);
        objectOutputStream.close();
        fileOutputStream.close();
    }

    @Override
    public void clearBinary() throws Exception {
        clearSaveFile(DataConstant.FILE_BINARY);
    }

    @Override
    public void loadBase64() throws Exception {
        final String dataBase64 = new String(Files.readAllBytes(Paths.get(DataConstant.FILE_BASE64)));
        final byte[] data = Base64.getDecoder().decode(dataBase64);

        final ByteArrayInputStream byteArrayInputStream = new ByteArrayInputStream(data);
        final ObjectInputStream objectInputStream = new ObjectInputStream(byteArrayInputStream);
        final Domain domain = (Domain) objectInputStream.readObject();
        serviceLocator.getDomainService().load(domain);
        objectInputStream.close();
    }

    @Override
    public void saveBase64() throws Exception {
        final Domain domain = prepareData();
        final File file = createSaveFile(DataConstant.FILE_BASE64);
        final ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        final ObjectOutputStream objectOutputStream = new ObjectOutputStream(byteArrayOutputStream);
        objectOutputStream.writeObject(domain);
        objectOutputStream.close();

        final byte[] bytes = byteArrayOutputStream.toByteArray();
        final String base64 = Base64.getEncoder().encodeToString(bytes);
        byteArrayOutputStream.close();

        final FileOutputStream fileOutputStream = new FileOutputStream(file);
        fileOutputStream.write(base64.getBytes());
        fileOutputStream.flush();
        fileOutputStream.close();
    }

    @Override
    public void clearBase64() throws Exception {
        clearSaveFile(DataConstant.FILE_BASE64);
    }

    @Override
    public void loadFasterJson() throws Exception {
        final String data = new String(Files.readAllBytes(Paths.get(DataConstant.FILE_FAST_JSON)));
        final ObjectMapper objectMapper = new ObjectMapper();
        final Domain domain = (Domain) objectMapper.readValue(data, Domain.class);

        serviceLocator.getDomainService().load(domain);
    }

    @Override
    public void saveFasterJson() throws Exception {
        @NotNull final Domain domain = prepareData();
        final File file = createSaveFile(DataConstant.FILE_FAST_JSON);
        final ObjectMapper objectMapper = new ObjectMapper();
        final String json = objectMapper.writerWithDefaultPrettyPrinter()
                .writeValueAsString(domain);

        final FileOutputStream fileOutputStream = new FileOutputStream(file);
        fileOutputStream.write(json.getBytes());
        fileOutputStream.flush();
        fileOutputStream.close();
    }

    @Override
    public void clearFasterJson() throws Exception {
        clearSaveFile(DataConstant.FILE_FAST_JSON);
    }

    @Override
    public void loadFasterXml() throws Exception {
        final String data = new String(Files.readAllBytes(Paths.get(DataConstant.FILE_FAST_XML)));

        final ObjectMapper objectMapper = new XmlMapper();
        final Domain domain = (Domain) objectMapper.readValue(data, Domain.class);

        serviceLocator.getDomainService().load(domain);
    }

    @Override
    public void saveFasterXml() throws Exception {
        final Domain domain = prepareData();
        final File file = createSaveFile(DataConstant.FILE_FAST_XML);

        final ObjectMapper objectMapper = new XmlMapper();
        final String xml = objectMapper.writerWithDefaultPrettyPrinter()
                .writeValueAsString(domain);

        final FileOutputStream fileOutputStream = new FileOutputStream(file);
        fileOutputStream.write(xml.getBytes());
        fileOutputStream.flush();
        fileOutputStream.close();
    }

    @Override
    public void clearFasterXml() throws Exception {
        clearSaveFile(DataConstant.FILE_FAST_XML);
    }

    @Override
    public void loadJaxBJson() throws Exception {
        final File file = new File(DataConstant.FILE_JAX_JSON);

        System.setProperty("javax.xml.bind.context.factory","org.eclipse.persistence.jaxb.JAXBContextFactory");
        JAXBContext jaxbContext = JAXBContext.newInstance(Domain.class);
        Unmarshaller un= jaxbContext.createUnmarshaller();

        un.setProperty(UnmarshallerProperties.MEDIA_TYPE, "application/json");
        un.setProperty(UnmarshallerProperties.JSON_INCLUDE_ROOT, true);

        Domain domain = (Domain) un.unmarshal(file);
        serviceLocator.getDomainService().load(domain);
    }

    @Override
    public void saveJaxBJson() throws Exception {
        final Domain domain = prepareData();
        final File file = createSaveFile(DataConstant.FILE_JAX_JSON);

        System.setProperty("javax.xml.bind.context.factory","org.eclipse.persistence.jaxb.JAXBContextFactory");

        JAXBContext jaxbContext = JAXBContext.newInstance(Domain.class);
        Marshaller mar= jaxbContext.createMarshaller();

        mar.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
        mar.setProperty(MarshallerProperties.MEDIA_TYPE, "application/json");
        mar.setProperty(MarshallerProperties.JSON_INCLUDE_ROOT, true);

        mar.marshal(domain, new File(DataConstant.FILE_JAX_JSON));
    }

    @Override
    public void clearJaxBJson() throws Exception {
        clearSaveFile(DataConstant.FILE_JAX_JSON);
    }

    @Override
    public void loadJaxBXml() throws Exception {
        final File file = new File(DataConstant.FILE_JAX_XML);
        JAXBContext jaxbContext = JAXBContext.newInstance(Domain.class);
        Unmarshaller un = jaxbContext.createUnmarshaller();
        Domain domain = (Domain) un.unmarshal(file);
        serviceLocator.getDomainService().load(domain);
    }

    @Override
    public void saveJaxBXml() throws Exception {
        @NotNull final Domain domain = prepareData();
        final File file = createSaveFile(DataConstant.FILE_JAX_XML);

        JAXBContext context = JAXBContext.newInstance(Domain.class);
        Marshaller mar= context.createMarshaller();
        mar.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
        mar.marshal(domain, new File(DataConstant.FILE_JAX_XML));
    }

    @Override
    public void clearJaxBXml() throws Exception {
        clearSaveFile(DataConstant.FILE_JAX_XML);
    }

    @Override
    public File createSaveFile(String fileName) throws Exception {
        final File file = new File(fileName);
        Files.deleteIfExists(file.toPath());
        Files.createFile(file.toPath());
        return file;
    }

    @Override
    public void clearSaveFile(String fileName) throws Exception {
        if (fileName == null || fileName.isEmpty()) return;
        final File file = new File(fileName);
        Files.deleteIfExists(file.toPath());
    }

    @NotNull
    private Domain prepareData() {
        @NotNull final Domain domain = new Domain();
        serviceLocator.getDomainService().export(domain);
        return domain;
    }

}
