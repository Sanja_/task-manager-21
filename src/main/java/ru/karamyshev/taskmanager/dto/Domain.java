package ru.karamyshev.taskmanager.dto;

import lombok.Getter;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;
import ru.karamyshev.taskmanager.entity.Project;
import ru.karamyshev.taskmanager.entity.Task;
import ru.karamyshev.taskmanager.entity.User;

import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

@Getter
@Setter
@XmlRootElement
public class Domain implements Serializable {

    @Nullable
    private List<Project> projects = new ArrayList<>();

    @Nullable
    private List<Task> tasks = new ArrayList<>();

    @Nullable
    private List<User> users = new ArrayList<>();

}
