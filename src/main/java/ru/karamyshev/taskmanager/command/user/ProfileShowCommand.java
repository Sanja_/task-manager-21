package ru.karamyshev.taskmanager.command.user;

import org.jetbrains.annotations.NotNull;
import ru.karamyshev.taskmanager.api.service.IAuthenticationService;
import ru.karamyshev.taskmanager.command.AbstractCommand;
import ru.karamyshev.taskmanager.entity.User;

public class ProfileShowCommand extends AbstractCommand {

    @NotNull
    @Override
    public String arg() {
        return "-shw-prfl";
    }

    @NotNull
    @Override
    public String name() {
        return "show-profile";
    }

    @NotNull
    @Override
    public String description() {
        return "Show profile.";
    }

    @Override
    public void execute() {
        IAuthenticationService authService = serviceLocator.getAuthenticationService();
        String currentLogin = authService.getCurrentLogin();
        String userId = authService.getUserId();
        System.out.println("[SHOW PROFILE]");
        User user = authService.showProfile(userId, currentLogin);
        System.out.println("LOGIN: " + user.getLogin());
        System.out.println("Role: " + user.getRole());
        System.out.println("HASH PASSWORD: " + user.getPasswordHash());
        System.out.println("[OK]");
    }

}
