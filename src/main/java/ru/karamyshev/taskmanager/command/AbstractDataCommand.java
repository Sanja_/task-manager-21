package ru.karamyshev.taskmanager.command;

import lombok.SneakyThrows;
import org.jetbrains.annotations.Nullable;
import ru.karamyshev.taskmanager.dto.Domain;

import java.io.Serializable;

public class AbstractDataCommand extends AbstractCommand implements Serializable {

    protected static final String FILE_BINARY = "./data.bin";
    protected static final String FILE_BASE64 = "./data.base64";
    protected static final String FILE_FAST_JSON = "./data-fast.json";
    protected static final String FILE_FAST_XML = "./data-fast.xml";
    protected static final String FILE_JAX_JSON = "./data-jax.json";
    protected static final String FILE_JAX_XML = "./data-jax.xml";

    @Nullable
    public Domain getDomain() {
        final Domain domain = new Domain();
        domain.setProjects(serviceLocator.getProjectService().getList());
        domain.setTasks(serviceLocator.getTaskService().getList());
        domain.setUsers(serviceLocator.getUserService().getList());
        return domain;
    }

    public void setDomain(final Domain domain) {
        if (domain == null) return;
        serviceLocator.getProjectService().getList().clear();
        serviceLocator.getProjectService().load(domain.getProjects());
        serviceLocator.getTaskService().getList().clear();
        serviceLocator.getTaskService().load(domain.getTasks());
        serviceLocator.getUserService().getList().clear();
        serviceLocator.getUserService().load(domain.getUsers());
    }

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @Nullable
    @Override
    public String name() {
        return null;
    }

    @Nullable
    @Override
    public String description() {
        return null;
    }

    @Override
    @SneakyThrows
    public void execute() {
    }

}
