package ru.karamyshev.taskmanager.command.task;

import org.jetbrains.annotations.NotNull;
import ru.karamyshev.taskmanager.api.service.IAuthenticationService;
import ru.karamyshev.taskmanager.api.service.ITaskService;
import ru.karamyshev.taskmanager.command.AbstractCommand;
import ru.karamyshev.taskmanager.entity.Task;
import ru.karamyshev.taskmanager.util.TerminalUtil;

public class TaskRemoveByIdCommand extends AbstractCommand {

    @NotNull
    @Override
    public String arg() {
        return "-tskrmvid";
    }

    @NotNull
    @Override
    public String name() {
        return "task-remove-by-id";
    }

    @NotNull
    @Override
    public String description() {
        return "Remove task by id.";
    }

    @Override
    public void execute() {
        IAuthenticationService authService = serviceLocator.getAuthenticationService();
        ITaskService taskService = serviceLocator.getTaskService();
        final String userId = authService.getUserId();
        System.out.println("[REMOVE TASK]");
        System.out.println("ENTER TASK ID FOR DELETION:");
        final String id = TerminalUtil.nextLine();
        final Task task = taskService.removeOneById(userId, id);
        if (task == null) System.out.println("[FAIL]");
        else System.out.println("[OK]");
    }

}
