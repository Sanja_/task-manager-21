package ru.karamyshev.taskmanager.command.project;

import org.jetbrains.annotations.NotNull;
import ru.karamyshev.taskmanager.api.service.IAuthenticationService;
import ru.karamyshev.taskmanager.api.service.IProjectService;
import ru.karamyshev.taskmanager.command.AbstractCommand;

public class ProjectClearCommand extends AbstractCommand {

    @NotNull
    @Override
    public String arg() {
        return "-prtclr";
    }

    @NotNull
    @Override
    public String name() {
        return "project-clear";
    }

    @NotNull
    @Override
    public String description() {
        return "Remove all projects.";
    }

    @Override
    public void execute() {
        IAuthenticationService authService = serviceLocator.getAuthenticationService();
        IProjectService projectService = serviceLocator.getProjectService();
        final String userId = authService.getUserId();
        System.out.println("[CLEAR PROJECT]");
        projectService.clear(userId);
        System.out.println("[OK]");
    }

}
