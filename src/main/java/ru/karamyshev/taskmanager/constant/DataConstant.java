package ru.karamyshev.taskmanager.constant;

public interface DataConstant {

    String FILE_BINARY = "./data.bin";

    String FILE_BASE64 = "./data.base64";

    String FILE_FAST_JSON = "./data-fast.json";

    String FILE_FAST_XML = "./data-fast.xml";

    String FILE_JAX_JSON = "./data-jax.json";

    String FILE_JAX_XML = "./data-jax.xml";

}
