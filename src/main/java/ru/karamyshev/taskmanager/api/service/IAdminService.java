package ru.karamyshev.taskmanager.api.service;

import org.jetbrains.annotations.Nullable;

import java.io.File;

public interface IAdminService {

    void loadBinary() throws Exception;

    void saveBinary() throws Exception;

    void clearBinary() throws Exception;

    void loadBase64() throws Exception;

    void saveBase64() throws Exception;

    void clearBase64() throws Exception;

    void loadFasterJson() throws Exception;

    void saveFasterJson() throws Exception;

    void clearFasterJson() throws Exception;

    void loadFasterXml() throws Exception;

    void saveFasterXml() throws Exception;

    void clearFasterXml() throws Exception;

    void loadJaxBJson() throws Exception;

    void saveJaxBJson() throws Exception;

    void clearJaxBJson() throws Exception;

    void loadJaxBXml() throws Exception;

    void saveJaxBXml() throws Exception;

    void clearJaxBXml() throws Exception;

    @Nullable
    File createSaveFile(@Nullable String fileName) throws Exception;

    void clearSaveFile(@Nullable String fileName) throws Exception;

}
