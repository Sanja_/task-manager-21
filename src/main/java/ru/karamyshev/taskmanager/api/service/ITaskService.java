package ru.karamyshev.taskmanager.api.service;

import org.jetbrains.annotations.Nullable;
import ru.karamyshev.taskmanager.entity.Task;

import java.util.List;

public interface ITaskService extends IService<Task> {

    void create(@Nullable String userId, @Nullable String name);

    void create(@Nullable String userId, @Nullable String name, @Nullable String description);

    void add(@Nullable String userId, @Nullable Task task);

    void remove(@Nullable String userId, @Nullable Task task);

    @Nullable
    List<Task> findAll(@Nullable String userId);

    void clear(@Nullable String userId);

    @Nullable
    Task findOneById(@Nullable String userId, @Nullable String id);

    @Nullable
    Task findOneByIndex(@Nullable String userId, @Nullable Integer index);

    @Nullable
    List<Task> findOneByName(@Nullable String userId, @Nullable String name);

    @Nullable
    Task removeOneById(@Nullable String userId, @Nullable String id);

    @Nullable
    Task removeOneByIndex(@Nullable String userId, @Nullable Integer index);

    @Nullable
    List<Task> removeOneByName(@Nullable String userId, @Nullable String name);

    @Nullable
    Task updateTaskById(@Nullable String userId, @Nullable String id, @Nullable String name, @Nullable String description);

    @Nullable
    Task updateTaskByIndex(@Nullable String userId, @Nullable Integer index, @Nullable String name, @Nullable String description);

}
