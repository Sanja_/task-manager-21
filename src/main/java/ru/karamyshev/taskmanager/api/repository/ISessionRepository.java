package ru.karamyshev.taskmanager.api.repository;

import org.jetbrains.annotations.Nullable;
import ru.karamyshev.taskmanager.entity.Session;

import java.util.List;

public interface ISessionRepository extends IRepository<Session> {

    void add(@Nullable String userId, @Nullable Session session);

    void remove(@Nullable String userId, @Nullable Session session);

    void clear(@Nullable String userId);

    @Nullable
    List<Session> findAllSession(@Nullable String userId);

    @Nullable
    Session findByUserId(@Nullable String userId) throws Exception;

    @Nullable
    Session findById(@Nullable String id) throws Exception;

    @Nullable
    Session removeByUserId(@Nullable String userId) throws Exception;

    boolean contains(@Nullable String id) throws Exception;
}
