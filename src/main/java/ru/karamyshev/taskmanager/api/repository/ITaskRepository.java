package ru.karamyshev.taskmanager.api.repository;

import org.jetbrains.annotations.NotNull;
import ru.karamyshev.taskmanager.entity.Task;

import java.util.List;

public interface ITaskRepository extends IRepository<Task> {

    void add(@NotNull String userId, @NotNull Task task);

    void remove(@NotNull String userId, @NotNull Task task);

    @NotNull
    List<Task> findAll(@NotNull String userId);

    void clear(@NotNull String userId);

    @NotNull
    Task findOneById(@NotNull String userId, @NotNull String id);

    @NotNull
    Task findOneByIndex(@NotNull String userId, @NotNull Integer index);

    @NotNull
    List<Task> findOneByName(@NotNull String userId, @NotNull String name);

    @NotNull
    Task removeOneById(@NotNull String userId, @NotNull String id);

    @NotNull
    Task removeOneByIndex(@NotNull String userId, @NotNull Integer index);

    @NotNull
    List<Task> removeOneByName(@NotNull String userId, @NotNull String name);

}
