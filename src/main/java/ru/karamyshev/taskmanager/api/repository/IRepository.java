package ru.karamyshev.taskmanager.api.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.karamyshev.taskmanager.entity.AbstractEntitty;

import java.util.List;

public interface IRepository<E extends AbstractEntitty> {

    @Nullable
    List<E> findAll();

    void clear();

    @Nullable
    List<E> getList();

    void add(@NotNull List<E> eList);

    void add(@NotNull E... e);

    void load(@NotNull List<E> eList);

}
