package ru.karamyshev.taskmanager.api.endpoint;

import ru.karamyshev.taskmanager.dto.Result;
import ru.karamyshev.taskmanager.entity.Session;

import javax.jws.WebMethod;
import javax.jws.WebParam;

public interface IAdminEndpoint {

    @WebMethod
    Result saveDataBinary(
            @WebParam(name = "session", partName = "session") Session session
    ) throws Exception;

    @WebMethod
    Result loadDataBinary(
            @WebParam(name = "session", partName = "session") Session session
    ) throws Exception;

    @WebMethod
    Result clearDataBinary(
            @WebParam(name = "session", partName = "session") Session session
    ) throws Exception;

    @WebMethod
    Result saveDataBase64(
            @WebParam(name = "session", partName = "session") Session session
    ) throws Exception;

    @WebMethod
    Result loadDataBase64(
            @WebParam(name = "session", partName = "session") Session session
    ) throws Exception;

    @WebMethod
    Result clearDataBase64(
            @WebParam(name = "session", partName = "session") Session session
    ) throws Exception;

    @WebMethod
    Result saveDataFasterJson(
            @WebParam(name = "session", partName = "session") Session session
    ) throws Exception;

    @WebMethod
    Result loadDataFasterJson(
            @WebParam(name = "session", partName = "session") Session session
    ) throws Exception;

    @WebMethod
    Result clearDataFasterJson(
            @WebParam(name = "session", partName = "session") Session session
    ) throws Exception;

    @WebMethod
    Result saveDataFasterXml(
            @WebParam(name = "session", partName = "session") Session session
    ) throws Exception;

    @WebMethod
    Result loadDataFasterXml(
            @WebParam(name = "session", partName = "session") Session session
    ) throws Exception;

    @WebMethod
    Result clearDataFasterXml(
            @WebParam(name = "session", partName = "session") Session session
    ) throws Exception;

    @WebMethod
    Result saveDataJaxBJson(
            @WebParam(name = "session", partName = "session") Session session
    ) throws Exception;

    @WebMethod
    Result loadDataJaxBJson(
            @WebParam(name = "session", partName = "session") Session session
    ) throws Exception;

    @WebMethod
    Result clearDataJaxBJson(
            @WebParam(name = "session", partName = "session") Session session
    ) throws Exception;

    @WebMethod
    Result saveDataJaxBXml(
            @WebParam(name = "session", partName = "session") Session session
    ) throws Exception;

    @WebMethod
    Result loadDataJaxBXml(
            @WebParam(name = "session", partName = "session") Session session
    ) throws Exception;

    @WebMethod
    Result clearDataJaxBXml(
            @WebParam(name = "session", partName = "session") Session session
    ) throws Exception;

}
