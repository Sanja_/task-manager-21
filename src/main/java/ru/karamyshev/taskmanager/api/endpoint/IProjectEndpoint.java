package ru.karamyshev.taskmanager.api.endpoint;

import org.jetbrains.annotations.Nullable;
import ru.karamyshev.taskmanager.entity.Project;

import java.util.List;

public interface IProjectEndpoint {

    void createNameProject(@Nullable String userId, @Nullable String name) throws Exception;

    void createDescriptionProject(
            @Nullable String userId,
            @Nullable String name,
            @Nullable String description
    ) throws Exception;

    void addProject(@Nullable String userId, @Nullable Project project);

    void removeProject(@Nullable String userId, @Nullable Project project);

    @Nullable
    List<Project> findAllProject(@Nullable String userId);

    void clearProject(@Nullable String userId);

    @Nullable
    Project findOneProjectByIndex(@Nullable String userId, @Nullable Integer index);

    @Nullable
    List<Project> findOneProjectByName(@Nullable String userId, @Nullable String name);

    @Nullable
    Project updateProjectById(
            @Nullable String userId,
            @Nullable String id,
            @Nullable String name,
            @Nullable String description
    );

    @Nullable
    Project removeOneProjectByIndex(@Nullable String userId, @Nullable Integer index);

    @Nullable
    List<Project> removeOneProjectByName(@Nullable String userId, @Nullable String name);

    @Nullable
    Project findOneProjectById(@Nullable String userId, @Nullable String id);

    @Nullable
    Project removeOneProjectById(@Nullable String userId, @Nullable String id);

    @Nullable
    Project updateProjectByIndex(
            @Nullable String userId,
            @Nullable Integer index,
            @Nullable String name,
            @Nullable String description
    );

    @Nullable
    List<Project> getProjectList();

    void loadProject(@Nullable List<Project> projects);
}
