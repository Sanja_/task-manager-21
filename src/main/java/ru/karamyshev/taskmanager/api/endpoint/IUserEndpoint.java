package ru.karamyshev.taskmanager.api.endpoint;

import org.jetbrains.annotations.Nullable;
import ru.karamyshev.taskmanager.entity.User;
import ru.karamyshev.taskmanager.enumerated.Role;

import java.util.List;

public interface IUserEndpoint {

    @Nullable
    User createUser(@Nullable String login, @Nullable String password);

    @Nullable
    User createUserEmail(@Nullable String login, @Nullable String password, @Nullable String email);

    @Nullable
    User createUserRole(
            @Nullable String login,
            @Nullable String password,
            @Nullable Role role
    );

    @Nullable
    User findUserById(@Nullable String id);

    @Nullable
    User findUserByLogin(@Nullable String login);

    @Nullable
    User removeUser(@Nullable User user);

    @Nullable
    User removeUserById(@Nullable String id);

    @Nullable
    User removeByLogin(@Nullable String login);

    @Nullable
    User lockUserByLogin(@Nullable String currentLogin, @Nullable String login);

    @Nullable
    User unlockUserByLogin(@Nullable String login);

    @Nullable
    User removeUserByLogin(@Nullable String currentLogin, @Nullable String login);

    @Nullable
    List<User> getUserList();

    void loadUser(@Nullable List<User> users);
}
