package ru.karamyshev.taskmanager.api.endpoint;

import org.jetbrains.annotations.Nullable;
import ru.karamyshev.taskmanager.entity.Task;

import javax.jws.WebMethod;
import java.util.List;

public interface ITaskEndpoint {

    @WebMethod
    void createName(@Nullable String userId, @Nullable String name);

    @WebMethod
    void createDescription(
            @Nullable String userId,
            @Nullable String name,
            @Nullable String description
    );

    @WebMethod
    void addTask(@Nullable String userId, @Nullable Task task);

    @WebMethod
    void removeTask(@Nullable String userId, @Nullable Task task);

    @Nullable
    @WebMethod
    List<Task> findAllTask(@Nullable String userId);

    @WebMethod
    void clearTask(@Nullable String userId);

    @Nullable
    @WebMethod
    Task findTaskOneById(@Nullable String userId, @Nullable String id);

    @Nullable
    @WebMethod
    Task findTaskOneByIndex(@Nullable String userId, @Nullable Integer index);

    @Nullable
    @WebMethod
    List<Task> findTaskOneByName(@Nullable String userId, @Nullable String name);

    @Nullable
    @WebMethod
    Task removeTaskOneById(@Nullable String userId, @Nullable String id);

    @Nullable
    @WebMethod
    Task removeTaskOneByIndex(@Nullable String userId, @Nullable Integer index);

    @Nullable
    @WebMethod
    List<Task> removeTaskOneByName(@Nullable String userId, @Nullable String name);

    @Nullable
    @WebMethod
    Task updateTaskById(
            @Nullable String userId,
            @Nullable String id,
            @Nullable String name,
            @Nullable String description
    );

    @Nullable
    @WebMethod
    Task updateTaskByIndex(
            @Nullable String userId,
            @Nullable Integer index,
            @Nullable String name,
            @Nullable String description
    );

    @Nullable
    @WebMethod
    List<Task> getTaskListT();

    @WebMethod
    void loadTask(@Nullable List<Task> tasks);

}
