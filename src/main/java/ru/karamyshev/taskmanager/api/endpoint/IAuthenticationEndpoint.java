package ru.karamyshev.taskmanager.api.endpoint;

import org.jetbrains.annotations.Nullable;
import ru.karamyshev.taskmanager.entity.User;
import ru.karamyshev.taskmanager.enumerated.Role;

import javax.jws.WebMethod;
import javax.jws.WebParam;

public interface IAuthenticationEndpoint {

    @WebMethod
    void login(
            @WebParam(name = "login") @Nullable String login,
            @WebParam(name = "password") @Nullable String password
    );

    @Nullable
    @WebMethod
    String getUserId();

    @WebMethod
    void checkRoles(@Nullable Role[] roles);

    @Nullable
    @WebMethod
    String getCurrentLogin();

    @WebMethod
    boolean isAuth();

    @WebMethod
    void registryUser(
            @Nullable String login,
            @Nullable String password,
            @Nullable String email
    );

    @WebMethod
    void renameLogin(
            @Nullable String userId,
            @Nullable String currentLogin,
            @Nullable String newLogin
    );

    @Nullable
    @WebMethod
    User showProfile(@Nullable String userId, @Nullable String login);

    @WebMethod
    void renamePassword(
            @Nullable String userId,
            @Nullable String currentLogin,
            @Nullable String oldPassword,
            @Nullable String newPassword
    );

    @WebMethod
    void logout();

}
