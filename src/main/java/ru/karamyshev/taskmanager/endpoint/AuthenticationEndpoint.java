package ru.karamyshev.taskmanager.endpoint;

import org.jetbrains.annotations.Nullable;
import ru.karamyshev.taskmanager.api.endpoint.IAuthenticationEndpoint;
import ru.karamyshev.taskmanager.api.service.IServiceLocator;
import ru.karamyshev.taskmanager.entity.User;
import ru.karamyshev.taskmanager.enumerated.Role;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;

@WebService
public class AuthenticationEndpoint implements IAuthenticationEndpoint {

    private IServiceLocator serviceLocator;

    public AuthenticationEndpoint() {
    }

    public AuthenticationEndpoint(final IServiceLocator serviceLocator) {
        this.serviceLocator = serviceLocator;
    }

    @Override
    @WebMethod
    public void login(
            @WebParam(name = "login") @Nullable String login,
            @WebParam(name = "password") @Nullable String password
    ) {
        serviceLocator.getAuthenticationService().login(login, password);
    }

    @Override
    @Nullable
    @WebMethod
    public String getUserId() {
        return serviceLocator.getAuthenticationService().getUserId();
    }

    @Override
    @WebMethod
    public void checkRoles(
            @WebParam(name = "role", partName = "role") @Nullable Role[] roles) {
        serviceLocator.getAuthenticationService().checkRoles(roles);
    }

    @Override
    @Nullable
    @WebMethod
    public String getCurrentLogin() {
        return serviceLocator.getAuthenticationService().getCurrentLogin();
    }

    @Override
    @WebMethod
    public boolean isAuth() {
        return serviceLocator.getAuthenticationService().isAuth();
    }

    @Override
    @WebMethod
    public void registryUser(
            @WebParam(name = "login", partName = "login") @Nullable String login,
            @WebParam(name = "password", partName = "password") @Nullable String password,
            @WebParam(name = "email", partName = "email") @Nullable String email
    ) {
        serviceLocator.getAuthenticationService().registry(login, password, email);
    }

    @Override
    @WebMethod
    public void renameLogin(
            @WebParam(name = "userId", partName = "userId") @Nullable String userId,
            @WebParam(name = "currentLogin", partName = "currentLogin") @Nullable String currentLogin,
            @WebParam(name = "newLogin", partName = "newLogin") @Nullable String newLogin
    ) {
        serviceLocator.getAuthenticationService().renameLogin(userId, currentLogin, newLogin);
    }

    @Nullable
    @Override
    @WebMethod
    public User showProfile(
            @WebParam(name = "userId", partName = "userId") @Nullable String userId,
            @WebParam(name = "login", partName = "login") @Nullable String login
    ) {
        return serviceLocator.getAuthenticationService().showProfile(userId, login);
    }

    @Override
    @WebMethod
    public void renamePassword(
            @WebParam(name = "userId", partName = "userId") @Nullable String userId,
            @WebParam(name = "currentLogin", partName = "currentLogin") @Nullable String currentLogin,
            @WebParam(name = "oldPassword", partName = "oldPassword") @Nullable String oldPassword,
            @WebParam(name = "newPassword", partName = "newPassword") @Nullable String newPassword
    ) {
        serviceLocator.getAuthenticationService().renamePassword(userId, currentLogin, oldPassword, newPassword);
    }

    @Override
    @WebMethod
    public void logout() {
        serviceLocator.getAuthenticationService().logout();
    }

}
