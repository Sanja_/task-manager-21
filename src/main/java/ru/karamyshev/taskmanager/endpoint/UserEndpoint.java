package ru.karamyshev.taskmanager.endpoint;

import org.jetbrains.annotations.Nullable;
import ru.karamyshev.taskmanager.api.endpoint.IUserEndpoint;
import ru.karamyshev.taskmanager.api.service.IServiceLocator;
import ru.karamyshev.taskmanager.entity.User;
import ru.karamyshev.taskmanager.enumerated.Role;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.util.List;

@WebService
public class UserEndpoint implements IUserEndpoint {

    private final IServiceLocator serviceLocator;

    public UserEndpoint(IServiceLocator serviceLocator) {
        this.serviceLocator = serviceLocator;
    }

    @Nullable
    @Override
    @WebMethod
    public User createUser(
            @WebParam(name = "login", partName = "login") @Nullable String login,
            @WebParam(name = "password", partName = "password") @Nullable String password
    ) {
        return serviceLocator.getUserService().create(login, password);
    }

    @Nullable
    @Override
    @WebMethod
    public User createUserEmail(
            @WebParam(name = "login", partName = "login") @Nullable String login,
            @WebParam(name = "password", partName = "password") @Nullable String password,
            @WebParam(name = "email", partName = "email") @Nullable String email
    ) {
        return serviceLocator.getUserService().create(login, password, email);
    }

    @Nullable
    @Override
    @WebMethod
    public User createUserRole(
            @WebParam(name = "login", partName = "login") @Nullable String login,
            @WebParam(name = "password", partName = "password") @Nullable String password,
            @WebParam(name = "role", partName = "role") @Nullable Role role
    ) {
        return serviceLocator.getUserService().create(login, password, role);
    }

    @Nullable
    @Override
    @WebMethod
    public User findUserById(
            @WebParam(name = "id", partName = "id") @Nullable String id
    ) {
        return serviceLocator.getUserService().findById(id);
    }

    @Nullable
    @Override
    @WebMethod
    public User findUserByLogin(
            @WebParam(name = "login", partName = "login") @Nullable String login
    ) {
        return serviceLocator.getUserService().findByLogin(login);
    }

    @Nullable
    @Override
    @WebMethod
    public User removeUser(
            @WebParam(name = "user", partName = "user") @Nullable User user
    ) {
        return serviceLocator.getUserService().removeUser(user);
    }

    @Nullable
    @Override
    @WebMethod
    public User removeUserById(
            @WebParam(name = "id", partName = "id") @Nullable String id
    ) {
        return serviceLocator.getUserService().removeById(id);
    }

    @Nullable
    @Override
    @WebMethod
    public User removeByLogin(
            @WebParam(name = "login", partName = "login") @Nullable String login
    ) {
        return serviceLocator.getUserService().removeByLogin(login);
    }

    @Nullable
    @Override
    @WebMethod
    public User lockUserByLogin(
            @WebParam(name = "currentLogin", partName = "currentLogin") @Nullable String currentLogin,
            @WebParam(name = "login", partName = "login") @Nullable String login
    ) {
        return serviceLocator.getUserService().lockUserByLogin(currentLogin, login);
    }

    @Nullable
    @Override
    @WebMethod
    public User unlockUserByLogin(
            @WebParam(name = "login", partName = "login") @Nullable String login
    ) {
        return serviceLocator.getUserService().unlockUserByLogin(login);
    }

    @Nullable
    @Override
    @WebMethod
    public User removeUserByLogin(
            @WebParam(name = "currentLogin", partName = "currentLogin") @Nullable String currentLogin,
            @WebParam(name = "login", partName = "login") @Nullable String login
    ) {
        return serviceLocator.getUserService().removeUserByLogin(currentLogin, login);
    }

    @Nullable
    @Override
    @WebMethod
    public List<User> getUserList() {
        return serviceLocator.getUserService().getList();
    }

    @Override
    @WebMethod
    public void loadUser(
            @WebParam(name = "users", partName = "users") @Nullable List<User> users) {
        serviceLocator.getUserService().load(users);
    }

}
