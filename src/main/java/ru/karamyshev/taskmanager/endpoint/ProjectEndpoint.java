package ru.karamyshev.taskmanager.endpoint;

import org.jetbrains.annotations.Nullable;
import ru.karamyshev.taskmanager.api.endpoint.IProjectEndpoint;
import ru.karamyshev.taskmanager.api.service.IServiceLocator;
import ru.karamyshev.taskmanager.entity.Project;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.util.List;

@WebService
public class ProjectEndpoint implements IProjectEndpoint {

    public ProjectEndpoint() {
    }

    private IServiceLocator serviceLocator;

    public ProjectEndpoint(final IServiceLocator serviceLocator) {
        this.serviceLocator = serviceLocator;
    }

    @Override
    @WebMethod
    public void createNameProject(
            @WebParam(name = "userId", partName = "userId") @Nullable String userId,
            @WebParam(name = "name", partName = "name") @Nullable String name
    ) throws Exception {
        serviceLocator.getProjectService().create(userId, name);
    }


    @Override
    @WebMethod
    public void addProject(
            @WebParam(name = "userId", partName = "userId") @Nullable String userId,
            @WebParam(name = "project", partName = "project") @Nullable Project project
    ) {
        serviceLocator.getProjectService().add(userId, project);
    }

    @Override
    @WebMethod
    public void removeProject(
            @WebParam(name = "userId", partName = "userId") @Nullable String userId,
            @WebParam(name = "project", partName = "project") @Nullable Project project
    ) {
        serviceLocator.getProjectService().remove(userId, project);
    }

    @Nullable
    @Override
    @WebMethod
    public List<Project> findAllProject(
            @WebParam(name = "userId", partName = "userId") @Nullable String userId
    ) {
        return serviceLocator.getProjectService().findAll(userId);
    }

    @Override
    @WebMethod
    public void clearProject(
            @WebParam(name = "userId", partName = "userId") @Nullable String userId
    ) {
        serviceLocator.getProjectService().clear(userId);
    }

    @Nullable
    @Override
    @WebMethod
    public Project findOneProjectByIndex(
            @WebParam(name = "userId", partName = "userId") @Nullable String userId,
            @WebParam(name = "projectIndex", partName = "projectIndex") @Nullable Integer index
    ) {
        return serviceLocator.getProjectService().findOneByIndex(userId, index);
    }

    @Nullable
    @Override
    @WebMethod
    public List<Project> findOneProjectByName(
            @WebParam(name = "userId", partName = "userId") @Nullable String userId,
            @WebParam(name = "projectName", partName = "projectName") @Nullable String name
    ) {
        return serviceLocator.getProjectService().findOneByName(userId, name);
    }

    @Nullable
    @Override
    @WebMethod
    public Project updateProjectById(
            @WebParam(name = "userId", partName = "userId") @Nullable String userId,
            @WebParam(name = "projectId", partName = "projectId") @Nullable String id,
            @WebParam(name = "projectUpName", partName = "projectUpName") @Nullable String name,
            @WebParam(name = "projectUpDescription", partName = "projectUpDescription") @Nullable String description
    ) {
        return serviceLocator.getProjectService().updateProjectById(userId, id, name, description);
    }

    @Nullable
    @Override
    @WebMethod
    public Project removeOneProjectByIndex(
            @WebParam(name = "userId", partName = "userId") @Nullable String userId,
            @WebParam(name = "projectIndex", partName = "projectIndex") @Nullable Integer index
    ) {
        return serviceLocator.getProjectService().removeOneByIndex(userId, index);
    }

    @Nullable
    @Override
    @WebMethod
    public List<Project> removeOneProjectByName(
            @WebParam(name = "userId", partName = "userId") @Nullable String userId,
            @WebParam(name = "projectName", partName = "projectName") @Nullable String name
    ) {
        return serviceLocator.getProjectService().removeOneByName(userId, name);
    }

    @Nullable
    @Override
    @WebMethod
    public Project findOneProjectById(
            @WebParam(name = "userId", partName = "userId") @Nullable String userId,
            @WebParam(name = "projectId", partName = "projectId") @Nullable String id
    ) {
        return serviceLocator.getProjectService().findOneById(userId, id);
    }

    @Nullable
    @Override
    @WebMethod
    public Project removeOneProjectById(
            @WebParam(name = "userId", partName = "userId") @Nullable String userId,
            @WebParam(name = "projectId", partName = "projectId") @Nullable String id
    ) {
        return serviceLocator.getProjectService().removeOneById(userId, id);
    }

    @Nullable
    @Override
    @WebMethod
    public Project updateProjectByIndex(
            @WebParam(name = "userId", partName = "userId") @Nullable String userId,
            @WebParam(name = "projectIndex", partName = "projectIndex") @Nullable Integer index,
            @WebParam(name = "projectName", partName = "projectName") @Nullable String name,
            @WebParam(name = "projectDescription", partName = "projectDescription") @Nullable String description
    ) {
        return serviceLocator.getProjectService().updateProjectByIndex(userId, index, name, description);
    }

    @Nullable
    @Override
    @WebMethod
    public List<Project> getProjectList() {
        return serviceLocator.getProjectService().getList();
    }


    @Override
    @WebMethod
    public void loadProject(
            @WebParam(name = "projects", partName = "projects") @Nullable List<Project> projects
    ) {
        serviceLocator.getProjectService().load(projects);
    }

    @Override
    @WebMethod
    public void createDescriptionProject(
            @WebParam(name = "userId", partName = "userId") @Nullable String userId,
            @WebParam(name = "name", partName = "name") @Nullable String name,
            @WebParam(name = "description", partName = "description") @Nullable String description
    ) throws Exception {
        serviceLocator.getProjectService().create(userId, name, description);
    }

}
