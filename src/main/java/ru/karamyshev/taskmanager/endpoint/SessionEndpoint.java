package ru.karamyshev.taskmanager.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.karamyshev.taskmanager.api.endpoint.ISessionEndpoint;
import ru.karamyshev.taskmanager.api.service.IServiceLocator;
import ru.karamyshev.taskmanager.dto.Fail;
import ru.karamyshev.taskmanager.dto.Result;
import ru.karamyshev.taskmanager.dto.Success;
import ru.karamyshev.taskmanager.entity.Session;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;


@WebService
public class SessionEndpoint implements ISessionEndpoint {

    private IServiceLocator serviceLocator;

    public SessionEndpoint() {
    }

    public SessionEndpoint(final IServiceLocator serviceLocator) {
        this.serviceLocator = serviceLocator;
    }

    @Nullable
    @Override
    @WebMethod
    public Session openSession(
            @WebParam(name = "login", partName = "login") final String login,
            @WebParam(name = "password", partName = "password") final String password
    ) {
        return serviceLocator.getSessionService().open(login, password);
    }


    @Nullable
    @Override
    @WebMethod
    public Result closeSession(
            @WebParam(name = "session", partName = "session") @Nullable final Session session
    ) throws Exception {
        serviceLocator.getSessionService().validate(session);
        try {
            serviceLocator.getSessionService().close(session);
            return new Success();
        } catch (Exception e) {
            return new Fail(e);
        }
    }

    @NotNull
    @Override
    @WebMethod
    public Result closeAllSession(
            @WebParam(name = "session", partName = "session") @Nullable final Session session
    ) throws Exception {
        serviceLocator.getSessionService().validate(session);
        try {
            serviceLocator.getSessionService().closeAll(session);
            return new Success();
        } catch (Exception e) {
            return new Fail(e);
        }
    }

}
