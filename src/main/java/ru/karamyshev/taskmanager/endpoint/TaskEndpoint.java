package ru.karamyshev.taskmanager.endpoint;

import org.jetbrains.annotations.Nullable;
import ru.karamyshev.taskmanager.api.endpoint.ITaskEndpoint;
import ru.karamyshev.taskmanager.api.service.IServiceLocator;
import ru.karamyshev.taskmanager.entity.Task;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.util.List;

@WebService
public class TaskEndpoint implements ITaskEndpoint {

    public TaskEndpoint() {
    }

    private IServiceLocator serviceLocator;

    public TaskEndpoint(IServiceLocator serviceLocator) {
        this.serviceLocator = serviceLocator;
    }

    @Override
    @WebMethod
    public void createName(
            @WebParam(name = "userI", partName = "userId") @Nullable String userId,
            @WebParam(name = "nameTask", partName = "nameTask") @Nullable String name
    ) {
        serviceLocator.getTaskService().create(userId, name);
    }

    @Override
    @WebMethod
    public void createDescription(
            @WebParam(name = "userId", partName = "userId") @Nullable String userId,
            @WebParam(name = "nameTask", partName = "nameTask") @Nullable String name,
            @WebParam(name = "descriptionTask", partName = "descriptionTask") @Nullable String description
    ) {
        serviceLocator.getTaskService().create(userId, name, description);
    }

    @Override
    @WebMethod
    public void addTask(
            @WebParam(name = "userId", partName = "userId") @Nullable String userId,
            @WebParam(name = "task", partName = "task") @Nullable Task task
    ) {
        serviceLocator.getTaskService().add(userId, task);
    }

    @Override
    @WebMethod
    public void removeTask(
            @WebParam(name = "userId", partName = "userId") @Nullable String userId,
            @WebParam(name = "task", partName = "task") @Nullable Task task
    ) {
        serviceLocator.getTaskService().remove(userId, task);
    }

    @Nullable
    @Override
    @WebMethod
    public List<Task> findAllTask(
            @WebParam(name = "userId", partName = "userId") @Nullable String userId
    ) {
        return serviceLocator.getTaskService().findAll(userId);
    }

    @Nullable
    @Override
    @WebMethod
    public void clearTask(
            @WebParam(name = "userId", partName = "userId") @Nullable String userId
    ) {
        serviceLocator.getTaskService().clear(userId);
    }

    @Nullable
    @Override
    @WebMethod
    public Task findTaskOneById(
            @WebParam(name = "userId", partName = "userId") @Nullable String userId,
            @WebParam(name = "taskId", partName = "taskId") @Nullable String id
    ) {
        return serviceLocator.getTaskService().findOneById(userId, id);
    }

    @Nullable
    @Override
    @WebMethod
    public Task findTaskOneByIndex(
            @WebParam(name = "userId", partName = "userId") @Nullable String userId,
            @WebParam(name = "taskIndex", partName = "taskIndex") @Nullable Integer index
    ) {
        return serviceLocator.getTaskService().findOneByIndex(userId, index);
    }

    @Nullable
    @Override
    @WebMethod
    public List<Task> findTaskOneByName(
            @WebParam(name = "userId", partName = "userId") @Nullable String userId,
            @WebParam(name = "taskName", partName = "taskName") @Nullable String name
    ) {
        return serviceLocator.getTaskService().findOneByName(userId, name);
    }

    @Nullable
    @Override
    @WebMethod
    public Task removeTaskOneById(
            @WebParam(name = "userId", partName = "userId") @Nullable String userId,
            @WebParam(name = "taskId", partName = "taskId") @Nullable String id
    ) {
        return serviceLocator.getTaskService().removeOneById(userId, id);
    }

    @Nullable
    @Override
    @WebMethod
    public Task removeTaskOneByIndex(
            @WebParam(name = "userId", partName = "userId") @Nullable String userId,
            @WebParam(name = "taskIndex", partName = "taskIndex") @Nullable Integer index
    ) {
        return serviceLocator.getTaskService().removeOneByIndex(userId, index);
    }

    @Nullable
    @Override
    @WebMethod
    public List<Task> removeTaskOneByName(
            @WebParam(name = "userId", partName = "userId") @Nullable String userId,
            @WebParam(name = "taskName", partName = "taskName") @Nullable String name
    ) {
        return serviceLocator.getTaskService().removeOneByName(userId, name);
    }

    @Nullable
    @Override
    @WebMethod
    public Task updateTaskById(
            @WebParam(name = "userId", partName = "userId") @Nullable String userId,
            @WebParam(name = "taskId", partName = "taskId") @Nullable String id,
            @WebParam(name = "taskName", partName = "taskName") @Nullable String name,
            @WebParam(name = "taskDescription", partName = "taskDescription") @Nullable String description) {
        return serviceLocator.getTaskService().updateTaskById(userId, id, name, description);
    }

    @Nullable
    @Override
    @WebMethod
    public Task updateTaskByIndex(
            @WebParam(name = "userId", partName = "userId") @Nullable String userId,
            @WebParam(name = "taskIndex", partName = "taskIndex") @Nullable Integer index,
            @WebParam(name = "taskName", partName = "taskName") @Nullable String name,
            @WebParam(name = "taskDescription", partName = "taskDescription") @Nullable String description
    ) {
        return serviceLocator.getTaskService().updateTaskByIndex(userId, index, name, description);
    }

    @Nullable
    @Override
    @WebMethod
    public List<Task> getTaskListT() {
        return serviceLocator.getTaskService().getList();
    }

    @Override
    @WebMethod
    public void loadTask(
            @WebParam(name = "tasks", partName = "tasks") @Nullable List<Task> tasks
    ) {
        serviceLocator.getTaskService().load(tasks);
    }

}
